import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import warehouse.entity.Computer;
import warehouse.entity.ComputerDto;
import warehouse.entity.DiscType;
import warehouse.entity.OperationSystem;
import warehouse.service.ComputerRepository;
import warehouse.service.ComputerServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ComputerServiceTest {

    @Mock
    private ComputerRepository computerRepo;

    @InjectMocks
    private ComputerServiceImpl computerService;

    @Test
    public void testSaveComputer() {
        Computer computer = new Computer(1, "Model", 500, DiscType.SSD, 16, 4, "AMD", 1000.0, OperationSystem.WINDOWS, "A123");
        Mockito.when(computerRepo.save(computer)).thenReturn(computer);
        ResponseEntity<?> responseEntity = computerService.saveComputer(computer);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @Test
    public void testDeleteComputerById() {
        long id = 1L;
        Mockito.doNothing().when(computerRepo).deleteById(id);
        ResponseEntity<?> responseEntity = computerService.deleteComputerById(id);

        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    public void testFindAllComputers() {
        List<Computer> computers = new ArrayList<>();
        computers.add(new Computer(1, "Model", 500, DiscType.SSD, 16, 4, "AMD", 1000.0, OperationSystem.WINDOWS, "A123"));
        Mockito.when(computerRepo.findAll()).thenReturn(computers);

        ResponseEntity<?> responseEntity = computerService.findAllComputers();
        List<ComputerDto> computerDtos = (List<ComputerDto>) responseEntity.getBody();

        assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
        assertEquals(computers.size(), computerDtos.size());
    }

    @Test
    public void testFindById() {
        long id = 1L;
        Computer computer = new Computer(1, "Model", 500, DiscType.SSD, 16, 4, "AMD", 1000.0, OperationSystem.WINDOWS, "A123");
        Mockito.when(computerRepo.findById(id)).thenReturn(Optional.of(computer));

        ResponseEntity<?> responseEntity = computerService.findById(id);
        ComputerDto computerDto = (ComputerDto) responseEntity.getBody();

        assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
        assertEquals(computer.getPrice(), computerDto.price());
    }

    @Test
    public void testFindAllByModel() {
        String model = "Model";
        List<Computer> computers = new ArrayList<>();
        computers.add(new Computer(1, "Model", 500, DiscType.SSD, 16, 4, "AMD", 1000.0, OperationSystem.WINDOWS, "A123"));
        Mockito.when(computerRepo.findAllByModel(model)).thenReturn(computers);

        ResponseEntity<?> responseEntity = computerService.findAllByModel(model);
        List<ComputerDto> computerDtos = (List<ComputerDto>) responseEntity.getBody();

        assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
        assertEquals(computers.size(), computerDtos.size());
        assertEquals(model, computerDtos.get(0).model());
    }

    @Test
    public void testFindAllByPriceBetween() {
        double firstPrice = 500.0;
        double secondPrice = 1500.0;
        List<Computer> computers = new ArrayList<>();
        computers.add(new Computer(1, "Model", 500, DiscType.SSD, 16, 4, "AMD", 1000.0, OperationSystem.WINDOWS, "A123"));
    }
}
