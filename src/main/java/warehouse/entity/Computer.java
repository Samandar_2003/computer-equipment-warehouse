package warehouse.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "computer")
@AllArgsConstructor
@NoArgsConstructor
public class Computer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "model", nullable = false)
    private String model;

    @Column(name = "memory", nullable = false)
    private int memory;

    @Column(name = "disc_type")
    @Enumerated(EnumType.STRING)
    private DiscType discType;

    @Column(name = "ram")
    private int ram;

    @Column(name = "video_card_memory")
    private int videoCardMemory;

    @Column(name = "vide_card")
    private String videoCard;

    @Column(name = "price", nullable = false)
    private double price;

    @Column(name = "os", nullable = false)
    private OperationSystem os;

    @Column(name = "serial_number")
    private String serialNumber;
}
