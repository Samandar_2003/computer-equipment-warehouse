package warehouse.entity;

public enum OperationSystem {

    WINDOWS, LINUX, MACOS, DOS
}
