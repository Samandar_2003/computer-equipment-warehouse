package warehouse.entity;

public record ComputerDto(
        long id,
        String model,
        int memory,
        String discType,
        int ram,
        int videoCardMemory,
        String videoCard,
        double price,
        String os
        ) {
}
