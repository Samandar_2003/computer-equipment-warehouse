package warehouse.service;

import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import warehouse.entity.Computer;

public interface ComputerService {

    ResponseEntity<?> saveComputer(Computer computer);

    ResponseEntity<?> deleteComputerById(long id);

    ResponseEntity<?> findAllComputers();

    ResponseEntity<?> findById(long id);

    ResponseEntity<?> findAllByModel(String model);

    ResponseEntity<?> findAllByPriceBetween(double firstPrice, double secondPrice);

    ResponseEntity<?> findAllAndSortByTemplate(String sortType, String sortBy);
}
