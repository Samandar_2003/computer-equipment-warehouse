package warehouse.service;

import org.springframework.data.jpa.repository.JpaRepository;
import warehouse.entity.Computer;

import java.util.List;

public interface ComputerRepository extends JpaRepository<Computer, Long> {

    List<Computer> findAllByModel(String model);
    List<Computer> findAllByPriceBetween(double first, double second);
}
