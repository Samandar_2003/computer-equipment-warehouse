package warehouse.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import warehouse.entity.Computer;
import warehouse.entity.ComputerDto;
import warehouse.entity.OperationSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ComputerServiceImpl implements ComputerService {

    private final ComputerRepository computerRepo;

    public ComputerServiceImpl(ComputerRepository computerRepo) {
        this.computerRepo = computerRepo;
    }

    @Override
    public ResponseEntity<?> saveComputer(Computer computer) {

        try {
            computerRepo.save(computer);
            return new ResponseEntity<>("SAVED", HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<?> deleteComputerById(long id) {

        try {
            computerRepo.deleteById(id);
            return new ResponseEntity<>("DELETED", HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<?> findAllComputers() {

        try {
            List<Computer> computers = computerRepo.findAll();
            List<ComputerDto> computerDtos = new ArrayList<>();

            for (Computer computer : computers) {
                computerDtos.add(computerMapper(computer));
            }

            return new ResponseEntity<>(computerDtos, HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<?> findById(long id) {

        try {
            Optional<Computer> optional = computerRepo.findById(id);

            if (optional.isEmpty()) {
                return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
            }

            ComputerDto computerDto = computerMapper(optional.get());
            return new ResponseEntity<>(computerDto, HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<?> findAllByModel(String model) {
        try {
            List<Computer> computers = computerRepo.findAllByModel(model);
            List<ComputerDto> computerDtos = new ArrayList<>();

            for (Computer computer : computers) {
                computerDtos.add(computerMapper(computer));
            }

            return new ResponseEntity<>(computerDtos, HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<?> findAllByPriceBetween(double firstPrice, double secondPrice) {
        try {
            List<Computer> computers = computerRepo.findAllByPriceBetween(firstPrice, secondPrice);
            List<ComputerDto> computerDtos = new ArrayList<>();

            for (Computer computer : computers) {
                computerDtos.add(computerMapper(computer));
            }

            return new ResponseEntity<>(computerDtos, HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<?> findAllAndSortByTemplate(String sortType, String sortBy) {
        try {
            return new ResponseEntity<>(findAllAndSort(sortType, sortBy), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    private List<Computer> findAllAndSort(String sortType, String sortBy) {
        if (sortType.equalsIgnoreCase("ASC")) {
            return computerRepo.findAll(Sort.by(sortBy).ascending());
        } else if (sortType.equalsIgnoreCase("DESC")) {
            return computerRepo.findAll(Sort.by(sortBy).descending());
        } else {
            return computerRepo.findAll();
        }
    }

    private ComputerDto computerMapper(Computer computer) {

        return new ComputerDto(
                computer.getId(),
                computer.getModel(),
                computer.getMemory(),
                computer.getDiscType().name(),
                computer.getRam(),
                computer.getVideoCardMemory(),
                computer.getVideoCard(),
                computer.getPrice(),
                computer.getOs().name()
        );
    }
}
