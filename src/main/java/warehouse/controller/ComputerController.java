package warehouse.controller;

import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import warehouse.entity.Computer;
import warehouse.service.ComputerService;

@RestController
@RequestMapping("/api/v1/computer")
public class ComputerController {

    private final ComputerService computerService;

    public ComputerController(ComputerService computerService){
        this.computerService = computerService;
    }

    @PostMapping("/save")
    public ResponseEntity<?> saveComputer(@RequestBody Computer computer){
        return computerService.saveComputer(computer);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteComputerById(@PathVariable long id){
        return computerService.deleteComputerById(id);
    }

    @GetMapping("/find-all")
    public ResponseEntity<?> findAll(){
        return computerService.findAllComputers();
    }

    @GetMapping("/find-by/{id}")
    public ResponseEntity<?> findById(@PathVariable long id){
        return computerService.findById(id);
    }

    @GetMapping("/find-by/model/{model}")
    public ResponseEntity<?> findByModel(@PathVariable String model){
        return computerService.findAllByModel(model);
    }

    @GetMapping("/find-by/price")
    public ResponseEntity<?> findByPrice(@RequestParam double first, @RequestParam double second){
        return computerService.findAllByPriceBetween(first, second);
    }

    @GetMapping("/order-by-template") //sortType: ASC, DESC
    public ResponseEntity<?> findALlOrderedByRegisterTime(@RequestParam String sortType, @RequestParam String sortBy) {
        return computerService.findAllAndSortByTemplate(sortType, sortBy);
    }
}
